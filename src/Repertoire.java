import java.util.Vector;

public class Repertoire extends Composant {

        private Vector<Composant> comp;

        public Repertoire(String nom) {
                super(nom);
                comp = new Vector<Composant>();
        }

        public boolean ajout(Composant c) {
                if (!estPere(c)) {
                        comp.addElement(c);
                        c.setPere(this);
                        return true;
                } else
                        return false;
        }

        @Override
        public int calculTaille() {
                int res = 0;
                for (int i = 0; i < comp.size(); i++) {
                        res += comp.elementAt(i).calculTaille();
                }
                return res;
        }
}



