
public class Main {

        public static void main(String[] args) {

                Fichier f1 = new Fichier(128, "f.txt");
                Fichier f2 = new Fichier(64, "f2.txt");
                Fichier f3 = new Fichier(32, "f3.txt");
                Repertoire R1 = new Repertoire("R1");
                Repertoire R2 = new Repertoire("R2");
                Repertoire R3 = new Repertoire("R3");
                R1.ajout(f1);
                R2.ajout(f2);
                R3.ajout(f3);
               
                System.out.println( Integer.toString(R1.calculTaille()));
                
                R1.ajout(R2);
               
                System.out.println( Integer.toString(R1.calculTaille()));
                
                R1.ajout(R1);
                
                System.out.println( Integer.toString(R1.calculTaille()) );
                
                R2.ajout(R3);
                
                System.out.println(Integer.toString(R1.calculTaille()) );
                
                R3.ajout(R1);
                
                System.out.println(Integer.toString(R1.calculTaille()) );
        }

}
