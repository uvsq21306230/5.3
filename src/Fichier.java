public class Fichier extends Composant {
        private int taille;

        public Fichier(int taille, String nom) {
                super(nom);
                this.taille = taille;
        }

        @Override
        public int calculTaille() {
                return taille;
        }
}
