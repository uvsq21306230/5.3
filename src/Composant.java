public abstract class Composant {

        protected String nom;
        protected boolean aPere;
        protected Repertoire pere;// pour verifier qu'un repertoire ne peut pas
                                                                // faire
                                                                // partie de ses descendants

        protected Composant(String nom) {
                this.nom = nom;
                this.aPere = false;
        }

        protected String getNom() {
                return nom;
        }

        protected void setPere(Repertoire P) {
                this.pere = P;
                this.aPere = true;
        }

        protected boolean estPere(Composant c) {

                if (this.nom == c.getNom())
                        return true;
                else {
                        if (this.aPere == true)
                                return this.pere.estPere(c);
                        else
                                return false;
                }
        }

        protected abstract int calculTaille();
}



